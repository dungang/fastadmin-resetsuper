<?php

namespace addons\resetsuper\command;

use app\admin\model\Admin;
use Exception;
use fast\Random;
use think\console\Input;
use think\console\input\Option;
use think\console\Output;
use think\console\Command;

class Resetsuper extends Command
{

    protected function configure()
    {
        parent::configure();
        $this->setName('resetsuper')
            ->addOption('username', 'u', Option::VALUE_REQUIRED, 'username', null)
            ->addOption('password', 'p', Option::VALUE_REQUIRED, 'super admin new password', null)
            ->setDescription('reset super admin password');
    }

    protected function execute(Input $input, Output $output)
    {
        $username = $input->getOption('username') ?: '';
        $password = $input->getOption('password') ?: '';
        if(empty($username)) {
            return $output->error("must input username");
        }
        if(empty($password)) {
            return $output->error("must input password");
        }
        $admin = Admin::get(['username' => $username]);
        if ($admin) {
            $admin->salt = Random::alnum();
            $admin->password = md5(md5($password) . $admin->salt);
            $admin->save();
            $output->info("update success text, username: " . $username . ', pasword:' . $password);
            $output->info("update success encrypt, salt: " . $admin->salt . ', pasword:' . $admin->password);
        } else {
            $output->error("admin not found, may be username not existed");
        }
    }
}
