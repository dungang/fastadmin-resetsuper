# 重置管理员密码

此插件请合法使用，不要用再违法的用途上！否则报警！

本插件的目的是，帮助部分fastadmin框架的使用者，忘记管理员密码的时候可以找回密码。

## 安全使用

- 非不得已不要生产服务器上安装本插件
- 建议在本地的项目链接远程的数据库，后再使用


## 下载代码

```shell
cd fastadmin/addons
git clone https://gitee.com/dungang/fastadmin-resetsuper.git resetsuper
```

## 远程修改管理员密码

- 先安装插件到本地
- 登陆后台清理所有缓存
- 修改本地代码链接远程项目的数据库
- 执行命令行即可

## 本地修改管理员密码

- 先安装插件到本地
- 登陆后台清理所有缓存
- 执行命令行即可

## 修改管理员密码的命令行

```shell
php think resetsuper -u 账号 -p 新密码

```

-u username 要修改的账号的用户名
-p password  新密码
